<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

  $messages[] = '';
  if (!empty($_COOKIE['save'])) {
    setcookie('save', '', 100000);
    $messages[] = 'Спасибо, результаты отправлены в базу данных.';
  }
  if (!empty($_COOKIE['notsave'])) {
    setcookie('notsave', '', 100000);
    $messages[] = 'Ошибка отправления в базу данных.';
  }

  $errors = array();
  // Ошибка имени, если пустая то записываем пустую строку, иначе ее значение из куки
  // Аналогично со всеми остальными
  $errors['fio'] = empty($_COOKIE['fio_error']) ? '' : $_COOKIE['fio_error'];
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['talents'] = !empty($_COOKIE['telents_error']);
  $errors['biography'] = !empty($_COOKIE['biography_error']);
  $errors['check'] = !empty($_COOKIE['check_error']);

  // name error print
  if ($errors['fio'] == 'null') {
    setcookie('fio_error', '', 100000);
    $messages[] = '<div>Заполните имя.</div>';
  }
  else if ($errors['fio'] == 'incorrect') {
      setcookie('fio_error', '', 100000);
      $messages[] = '<div>Недопустимые символы. Введите имя заново.</div>';
  }

  // email error print
  if ($errors['email']) {
    setcookie('email_error', '', 100000);
    $messages[] = '<div>Заполните почту.</div>';
  }

  // talentss error print
  if ($errors['talents']) {
    setcookie('talents_error', '', 100000);
    $messages[] = '<div>Выберите хотя бы одну сверхспособность.</div>';
  }

  if ($errors['biography']) {
    setcookie('biography_error', '', 100000);
    $messages[] = '<div>Напишите что-нибудь о себе.</div>';
  }

  if ($errors['check']) {
    setcookie('check_error', '', 100000);
    $messages[] = '<div>Вы не можете отправить форму не согласившись с контрактом.</div>';
  }

  $values = array();
  $talents = array();
  
  $talents['1'] = "Скорочтение";
  $talents['2'] = "Хорошая физическая форма";
  $talents['3'] = "Фотографическая память";

  $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['data'] = empty($_COOKIE['data_value']) ? '' : $_COOKIE['data_value'];
  $values['radio1'] = empty($_COOKIE['radio1_value']) ? '' : $_COOKIE['radio1_value'];
  $values['radio2'] = empty($_COOKIE['radio2_value']) ? '' : $_COOKIE['radio2_value'];
  $values['biography'] = empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];
//////непоняточка
if (!empty($_COOKIE['talents_value'])) {
  $talents_value = json_decode($_COOKIE['talents_value']);
}
$values['talents'] = [];
if (isset($talents_value) && is_array($talents_value)) {
  foreach ($talents_value as $power) {
      if (!empty($talents[$power])) {
          $values['talents'][$power] = $power;
      }
  }
}




  // $talents = empty($_COOKIE['talents']) ? 

  include('form.php');
}

else {
  // Проверяем ошибки.
  $errors = FALSE;
  if (empty($_POST['fio'])) {

    // Выдаем куку на день с флажком об ошибке в поле name.
    setcookie('fio_error', 'null', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else if (!preg_match("#^[aA-zZ0-9-]+$#", $_POST["fio"])) {
      setcookie('fio_error', 'incorrect', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['email'])) {
    // Выдаем куку на день с флажком об ошибке в поле name.
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
  }



  if (empty($_POST['biography'])) {
    // Выдаем куку на день с флажком об ошибке в поле name.
    setcookie('biography_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['check'])) {
    // Выдаем куку на день с флажком об ошибке в поле name.
    setcookie('check_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  $talents = array();
  foreach ($_POST['talents'] as $key => $value) {
      $talents[$key] = $value;
  }
  if (!sizeof($talents)) {
    setcookie('talents_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('talents_value', json_encode($talents), time() + 30 * 24 * 60 * 60);
  }

  setcookie('data_value', $_POST['data'], time() + 30 * 24 * 60 * 60);
  setcookie('radio1_value', $_POST['radio1'], time() + 30 * 24 * 60 * 60);
  setcookie('radio2_value', $_POST['radio2'], time() + 30 * 24 * 60 * 60);


  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }

  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('talents_error', '', 100000);
    setcookie('biography_error', '', 100000);
    setcookie('check_error', '', 100000);

  }

$name = $_POST['fio'];
$email = $_POST['email'];
$data = $_POST['data'];
$gender = $_POST['radio1'];
$radio2 = $_POST['radio2'];
$biography = $_POST['biography'];
$talents = array();
$talents = $_POST['talents'];
//Сохранение в базу данных.
try{
$db = new PDO('mysql:host=localhost;dbname=u35649', 'u35649', '35734534', array(PDO::ATTR_PERSISTENT => true));
//Подготовленный запрос. Не именованные метки.
  $user = $db->prepare("INSERT INTO user set name = ?, email = ?, date = ?, gender = ?, amountOFColumn = ?, biography = ?");
  $user -> execute([$name, $email, $data, $gender, $radio2, $biography]);
  $userId = $db->lastInsertId();
  
  $userPower = $db->prepare("INSERT INTO userPower (userId, powerId) VALUES (:userId, :powerId)");
  foreach($talents as $ability) {
    $userPower -> execute(array('userId' => $userId, 'powerId' => $ability));
  }

  setcookie('save', '1');
}
 catch (PDOException $e) {
    printf($e);
    setcookie('notsave', '1');
}

header('Location: ?save=1');
}
?>


